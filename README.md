
### Overview

#### Exercice 1 (alphabetPosition)

RESTful APIs pour soutenir la fonction de alphabetPosition

`POST /words` pour enregistrer un string (word) avec son format alphabetPosition
```
{
	"value": "The sunset sets at twelve o' clock."
}
```
*Par souci de simplicité, l'application ne nous permet pas d'enregistrer plus d'un word.*

`GET /words/first` pour lire le word enregistré avec son format alphabetPosition.

*le corps de la réponse est composé de id = 0, value = {word enregistré}, alphabetPosition = {positions dans l'alphabet de chaque caractère du word, séparées par un espace}.*

`DELETE /words/all` pour supprimer le word enregistré afin d'enregistrer un autre.

*Dans le cas où nous essayions d'enregistrer un deuxième word en utilisant la méthode POST, nous obtiendrions une erreur car un seul word peut être enregistré à la fois*

#### Exercice 2 (maticre transposée)

RESTful APIs pour soutenir la fonction qui transpose la matrice

`POST /matrices` pour enregistrer une matrice avec son format transposée
```
{
	"value": [[1,2,3],[4,5,6]]
}
```
*Par souci de simplicité, l'application ne nous permet pas d'enregistrer plus d'une matrice.*

`GET /matrices/first` pour lire la matrice enregistré avec son format transposée.

*le corps de la réponse est composé de id = 0, value = {matrice enregistrée}, transpose = {format transposée de la matrice}.*

`DELETE /matrices/all` pour supprimer la matrice enregistré afin d'enregistrer une autre.

*Dans le cas où nous essayions d'enregistrer une deuxième matrice avec la méthode POST, nous obtiendrions une erreur car une seul matrice peut être enregistré à la fois*

#### Exercice 3 (formatter l'échantillon en 2 résultats distincts)

RESTful APIs pour soutenir les 2 fonctions de formatage de l'échantillon

`GET /reviews/v1` pour obtenir l'échantillon dans le premier format.

`GET /reviews/v2` pour obtenir l'échantillon dans le second format.

*L'échantillon est déjà enregistré dans l'application, il suffit donc de le lire dans le format desiré avec les deux API ci-dessus*

### Lancement de l'application

Pour lancer l'application, nous devons:

1. Installer toutes les dépendances `npm install`
2. Lancer le serveur `npm run start`

### Notes

*L'application dispose de fichiers supplémentaires pour prendre en charge les connexions à la base de données et la configuration de l'application. Ils ne sont pas utilisés pour ce projet. Cependant, j'ai décidé de les conserver au cas où je devrais ajouter ces fonctionnalités dans l'avenir.*

*Le projet est initialisé avec: eslint, jest, jest, husky et la possibilité de définir des variables d'environnement*

### References

Le projet a été developpé en se basant sur le lien de Medium suivant:
https://medium.com/swlh/advanced-koa-js-boilerplate-bda90c9abe24