'use strict';

const controller = require('./review.controller');

module.exports = Router => {
  const router = new Router({
    prefix: `/reviews`,
  });

  router.get('/v1', controller.getFormat1).get('/v2', controller.getFormat2);

  return router;
};
