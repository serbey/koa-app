'use strict';

const formatReviews = require('../../utils/formatReviews.util');

/**
 * Mock database, replace this with your db models import, required to perform query to your database.
 */
const db = {
  reviews: [
    {
      description: 'Ceci est un joli logement',
      summary: 'très joli',
      headline: 'Joli Logement',
      lang: 'fr',
    },
    {
      description: 'this is a pretty house',
      summary: 'really pretty',
      headline: 'Pretty House',
      lang: 'en',
    },
    {
      description: 'Esta una casa muy guapa',
      summary: 'muy guapa',
      headline: 'Guapa Casa',
      lang: 'es',
    },
    {
      description: '這是一個可愛的房子',
      summary: '非常好的房子',
      headline: '漂亮的房子',
      lang: 'cn',
    },
  ],
};

exports.getFormat1 = async ctx => {
  ctx.status = 200;
  ctx.body = formatReviews.firstFunction(db.reviews);
};

exports.getFormat2 = async ctx => {
  ctx.status = 200;
  ctx.body = formatReviews.secondFunction(db.reviews);
};
