'use strict';

const matrixTranspose = require('../../utils/matrixTranspose.util');

/**
 * Mock database, replace this with your db models import, required to perform query to your database.
 */
const db = {
  matrices: [
    /*
    {
      id: 0,
      value: [[1,2,3],[4,5,6]],
      transpose: [[1,4],[2,5],[3,6]]
    }
    */
  ],
};

exports.getFirst = async ctx => {
  const matrix = db.matrices[0];
  ctx.assert(matrix, 404, 'matrices database is empty');
  ctx.status = 200;
  ctx.body = matrix;
};

exports.createOne = async ctx => {
  const { value } = ctx.request.body;
  ctx.assert(value, 400, 'missing value');
  ctx.assert(value[0].constructor === Array, 400, 'value must be 2D array');
  ctx.assert(
    db.matrices.length === 0,
    409,
    `matrix already exists, can't insert more than one`,
  );
  const id = 0;
  const newMatrix = {
    id,
    value,
    transpose: matrixTranspose(value),
  };
  db.matrices.push(newMatrix);
  const createdMatrix = db.matrices.find(matrix => matrix.id === id);
  ctx.status = 201;
  ctx.body = createdMatrix;
};

exports.removeAll = async ctx => {
  db.matrices = [];
  ctx.status = 200;
  ctx.body = {
    status: 'success',
  };
};
