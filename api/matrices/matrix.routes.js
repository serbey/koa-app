'use strict';

const controller = require('./matrix.controller');

module.exports = Router => {
  const router = new Router({
    prefix: `/matrices`,
  });

  router
    .get('/first', controller.getFirst)
    .post('/', controller.createOne)
    .delete('/all', controller.removeAll);

  return router;
};
