'use strict';

const alphabetPosition = require('../../utils/alphabetPosition.util');

/**
 * Mock database, replace this with your db models import, required to perform query to your database.
 */
const db = {
  words: [
    /*
    {
      id: 0,
      value: "The sunset sets at twelve o' clock.",
      alphabetPosition: "20 8 5 19 21 14 19 5 20 19 5 20 19 1 20 20 23 5 12 22 5 15 3 12 15 3 11"
    }
    */
  ],
};

exports.getFirst = async ctx => {
  const word = db.words[0];
  ctx.assert(word, 404, 'words database is empty');
  ctx.status = 200;
  ctx.body = word;
};

exports.createOne = async ctx => {
  const { value } = ctx.request.body;
  ctx.assert(value, 400, 'missing value');
  ctx.assert(
    db.words.length === 0,
    409,
    'a word is already present in the database',
  );
  const id = 0;
  const newWord = {
    id,
    value,
    alphabetPosition: alphabetPosition(value),
  };
  db.words.push(newWord);
  const createdWord = db.words.find(word => word.id === id);
  ctx.status = 201;
  ctx.body = createdWord;
};

exports.removeAll = async ctx => {
  db.words = [];
  ctx.status = 200;
  ctx.body = {
    status: 'success',
  };
};
