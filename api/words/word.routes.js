'use strict';

const controller = require('./word.controller');

module.exports = Router => {
  const router = new Router({
    prefix: `/words`,
  });

  router
    .get('/first', controller.getFirst)
    .post('/', controller.createOne)
    .delete('/all', controller.removeAll);

  return router;
};
