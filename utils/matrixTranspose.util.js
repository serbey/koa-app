module.exports = matrix => {
  let outerArray = [];
  const jMax = matrix.length;
  let i = 0;
  const iMax = matrix[0].length;
  for (; i < iMax; i++) {
    let innerArray = [];
    let j = 0;
    for (; j < jMax; j++) {
      innerArray.push(matrix[j][i]);
    }
    outerArray.push(innerArray);
  }
  return outerArray;
};
