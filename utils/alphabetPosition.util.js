module.exports = word => {
  const lowerCaseWord = word.toLowerCase();
  let result = '';
  let i = 0;
  const iMax = lowerCaseWord.length;
  for (; i < iMax; i++) {
    const char = lowerCaseWord.charCodeAt(i);
    if (char >= 97 && char <= 122) {
      result += char - 96 + ' ';
    }
  }
  return result.slice(0, -1);
};
