const firstFunction = reviews => {
  const result = {};
  for (const review of reviews) {
    result[review.lang] = {
      description: review.description,
      summary: review.summary,
      headline: review.headline,
    };
  }
  return result;
};

const secondFunction = reviews => {
  const result = {};
  for (const review of reviews) {
    if (['en', 'fr'].includes(review.lang)) {
      result['description_' + review.lang] = review.description;
      result['summary_' + review.lang] = review.summary;
    }
  }
  return result;
};

module.exports = {
  firstFunction,
  secondFunction,
};
